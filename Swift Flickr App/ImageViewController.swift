//
//  ImageViewController.swift
//  Swift Flickr App
//
//  Created by Francisco Morales on 10/20/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var browsingImage: UIImageView!
    var newImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 6.0
        
        self.browsingImage.image = self.newImage

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.browsingImage
    }

}
