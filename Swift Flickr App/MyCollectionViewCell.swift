//
//  MyCollectionViewCell.swift
//  Swift Flickr App
//
//  Created by Francisco Morales on 10/18/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {

    @IBOutlet var myImageView: UIImageView!
    @IBOutlet var myLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
