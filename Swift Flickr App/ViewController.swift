//
//  ViewController.swift
//  Swift Flickr App
//
//  Created by Francisco Morales on 10/18/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, UITextViewDelegate {

    //Flickr API Key
    let apiKey = "1dd17dde0fed7286935d83875fcc17dd"
    
    //Flickr Results Array storage
    var flickrResults = [AnyObject]() //flickrPhotos
    var imageData = UIImage()
    
    //Outlets from ViewController storyboard
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var searchTextField: UITextField!
    
    //Store SearchTextField text in this String variable
    var searchString: String = ""
    
    
    //VIEWDIDLOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchTextField.delegate = self
        
        //Initiating Collection View Cells
        self.collectionView.register(UINib(nibName: "MyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
    }//End ViewDidLoad

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //SEARCH BUTTON IN NAV - If user presses button, it takes textfield text and calls "performSearch()"
    @IBAction func searchButtonTapped(_ sender: UIBarButtonItem) {
        searchString = searchTextField.text!
        searchTextField.text = ""
        searchTextField.resignFirstResponder()
        performSearch()
        
    }
    
    //HIDE KEYBOARD - If user presses return; If user presses return key, it takes textfield text and calls "performSearch()"
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //searchTextField.resignFirstResponder()
        searchString = searchTextField.text!
        searchTextField.text = ""
        searchTextField.resignFirstResponder()
        performSearch()
        return (true)
    }
    
    //HIDE KEYBOARD - If user touches outside keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //PERFORM SEARCH - Takes user string from text field and gets results
    func performSearch() {

        let url = flickrSearchURLForSearchTerm(searchString)

        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }

            let json = try! JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
            print(json)

            let photos = json["photos"] as! Dictionary<String, Any>
            self.flickrResults = photos["photo"] as! [AnyObject]

            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }

        }

        task.resume()
    }
    
    //1 = NUMBER OF SECTIONS
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2 - NUMBER OF ITEMS IN SECTION
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return (flickrResults as AnyObject).count
    }
    
    //3 - CELL FOR ITEM AT
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                      for: indexPath) as! MyCollectionViewCell
        let farm = flickrResults[indexPath.row]["farm"]
        let farm1 = farm as! NSNumber
        let farm2 = farm1.stringValue
        
        let x = "https://farm" + farm2 + ".staticflickr.com/" + ((flickrResults[indexPath.row]["server"]) as! String) + "/" + ((flickrResults[indexPath.row]["id"]) as! String) + "_" + ((flickrResults[indexPath.row]["secret"]) as! String) + ".jpg"
        
        let url = URL(string: x)
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                cell.myImageView.image = UIImage(data: data!)
            }
        }

        cell.myLabel.backgroundColor = UIColor.white
        cell.myLabel.text = flickrResults[indexPath.row]["title"] as? String
        
        return cell
    }
    //4 - SIZE FOR ITEM AT
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/2 - 10, height: self.view.frame.size.width/2 - 0)
    }
    //5 - DID SELECT AT
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                      for: indexPath) as! MyCollectionViewCell
        let farm = flickrResults[indexPath.row]["farm"]
        let farm1 = farm as! NSNumber
        let farm2 = farm1.stringValue
        
        let x = "https://farm" + farm2 + ".staticflickr.com/" + ((flickrResults[indexPath.row]["server"]) as! String) + "/" + ((flickrResults[indexPath.row]["id"]) as! String) + "_" + ((flickrResults[indexPath.row]["secret"]) as! String) + ".jpg"
        
        let url = URL(string: x)
        
//        DispatchQueue.global().async {
//            let data = try? Data(contentsOf: url!)
//            DispatchQueue.main.async {
//                //Storing image to imageData global variable
//                self.imageData = UIImage(data: data!)!
//            }
//        }
        
        let data = try? Data(contentsOf: url!)
        self.imageData = UIImage(data: data!)!
        
        self.performSegue(withIdentifier: "toImageViewController", sender: imageData)
    }
    //6 - PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toImageViewController" {
            let destination = segue.destination as! ImageViewController
            destination.newImage = imageData

        }
    }
    
    //URL SEARCH TERM FORMATTER
    fileprivate func flickrSearchURLForSearchTerm(_ searchTerm:String) -> URL? {
        
        guard let escapedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
            return nil
        }
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=20&format=json&nojsoncallback=1"
        
        guard let url = URL(string:URLString) else {
            return nil
        }
        
        return url
    }

}

